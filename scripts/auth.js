const guideList = document.getElementById('guide-list')
const loggedIn = document.querySelectorAll('.logged-in')
const loggedOut = document.querySelectorAll('.logged-out')

const getListItems = (item) => {
    let html = document.createElement('li')
    const itemList = 
    `
        <div class="collapsible-header grey lighten-4">${item.title}</div>
        <div class="collapsible-body white">${item.description}</div>
    `
    html.innerHTML = itemList
    guideList.append(html)
}

const setupUI = (user) => {
    const accountDetails = document.getElementById('account-details')
    if(user){
        const html = 
        `
            <p>Logged in: ${user.email}</p>
        `
        accountDetails.innerHTML = html
    }
    else {
        const html = `` 
        accountDetails.innerHTML = html
    }
        loggedIn.forEach(item => {
            if(user){
                item.style.display = "block"
            }
            else {
                item.style.display = "none"
            }
        })
    
}

// Listen for auth status changes
auth.onAuthStateChanged(user => {
    if(user){
        // Getting data of firebase
        db.collection('list').onSnapshot(list => {
                list.docs.forEach(item => {
                    getListItems(item.data())
                })
            })
    }
    else {
        guideList.parentNode.innerHTML = `<h2>Please login to see the lists</h2>`
    }
    setupUI(user)
})

// Sing up / Registrarse
const signupForm = document.querySelector('#signup-form')

signupForm.addEventListener('submit', (e) => {
    e.preventDefault()

    const mail = signupForm['signup-email'].value
    const password = signupForm['signup-password'].value

    auth.createUserWithEmailAndPassword(mail, password).then(cred => {
        return db.collection('users').doc(cred.user.id).set({
            bio: singupForm['signup-bio'].value
        })
    }).then(() => {
        const modal = document.querySelector('#modal-signup')
        M.Modal.getInstance(modal).close()
        signupForm.reset()
    })
})

// Logout / Cerrar sesión
const logout = document.getElementById('logout')
logout.addEventListener('click', e => {
    e.preventDefault()
    auth.signOut().then(() => {
        console.log('User signed out')
    })
})

// Login / Iniciar sesión
const loginForm = document.getElementById('login-form')
loginForm.addEventListener('submit', (e) => {
    e.preventDefault()

    const email = loginForm['login-email'].value
    const password = loginForm['login-password'].value

    auth.signInWithEmailAndPassword(email, password).then(cred => {
        //console.log(cred.user)

        const modal = document.getElementById('modal-login')
        M.Modal.getInstance(modal).close()
        loginForm.reset()
    })
})

// Create new guide
const createForm = document.getElementById("create-form")
createForm.addEventListener("submit", (e) => {
    e.preventDefault()
    db.collection('list').add({
        title: e.target.title.value,
        description: e.target.content.value
    }).catch(error => {
        console.log(error.message)
    })

    const modal = document.getElementById('modal-create')
    M.Modal.getInstance(modal).close()
    createForm.reset()
})