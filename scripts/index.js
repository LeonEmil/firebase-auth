document.addEventListener('DOMContentLoaded', () => {
    let modals = document.querySelectorAll('.modal')
    M.Modal.init(modals)

    let items = document.querySelectorAll('.collapsible')
    M.Collapsible.init(items) 
})